import { Component, OnDestroy, OnInit } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { timer } from 'rxjs';
import { SubSink } from 'subsink';

import { StorageService } from './shared/storage.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  private subs = new SubSink();

  constructor(
    private storage: StorageService,
    private splash: SplashScreen) { }

  ngOnInit(): void {
    this.splash.show();
    this.subs.sink = timer(5000).subscribe(_ => this.splash.hide());
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
