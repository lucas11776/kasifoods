import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';

import { CheckoutRoutingModule } from './checkout-routing.module';
import { CheckoutPageComponent } from './checkout.page.component';
import { CreditCardPage } from './pages/credit-card/credit-card.page';
import { MomoPage } from './pages/momo/momo.page';
import { CollectionPage } from './pages/collection/collection.page';


@NgModule({
  declarations: [
    CheckoutPageComponent,
    CreditCardPage,
    MomoPage,
    CollectionPage,
  ],
  imports: [
    CommonModule,
    CheckoutRoutingModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    RxReactiveFormsModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CheckoutModule { }
