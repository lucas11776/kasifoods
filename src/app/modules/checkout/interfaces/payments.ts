/* eslint-disable @typescript-eslint/naming-convention */

export const enum PaymentMethods {
	'creditCard', 'momo', 'collection'
}

export interface Transaction {
	id: number;
	payment_method: string;
	payment_id: string;
	amount: string;
	status: string;
	created_at: string;
	updated_at: string;
}

export interface MomoPayment {
	cellphone_number: string;
}
