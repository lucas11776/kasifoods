import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { MomoPayment } from '../interfaces/payments';
import { Order } from '../../orders/interfaces/order';
import { API } from '../../../../environments/environment';
import { HttpService } from '../../../shared/http.service';

@Injectable({
  providedIn: 'root'
})
export class MomoService {
  constructor(private http: HttpService) { }

  pay(momo: MomoPayment): Observable<Order> {
    return this.http.post(`${API}/checkout/momo`, momo);
  }
}
