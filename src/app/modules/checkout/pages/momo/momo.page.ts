/* eslint-disable @typescript-eslint/naming-convention */
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SubSink } from 'subsink';

import { MomoService } from '../../shared/momo.service';
import { ToastService } from '../../../../shared/toast.service';
import { LoaderService } from '../../../../shared/loader.service';

@Component({
  templateUrl: './momo.page.html',
  styleUrls: ['./momo.page.css']
})
export class MomoPage implements OnInit, OnDestroy {
  form: FormGroup;

  private subs = new SubSink();

  constructor(
    private formBuilder: FormBuilder,
    private loader: LoaderService,
    private momo: MomoService,
    private toast: ToastService,
    private router: Router) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      cellphone_number: [null, [Validators.required, Validators.minLength(10), Validators.maxLength(14)]]
    });
  }

  pay(): void {
    this.loader.show('Processing Payment...');
    this.subs.sink = this.momo.pay(this.form.value).subscribe(
      order => {
        this.loader.dismiss();
        this.router.navigate(['/tabs', 'orders', order.id]);
      },
      error => {
        this.loader.dismiss();
        this.toast.show(error.error.errors.cellphone_number ?? error.message);
        console.log('Momo payment error: ', error);
      }
    );
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
