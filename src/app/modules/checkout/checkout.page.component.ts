import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { PaymentMethods } from './interfaces/payments';
import { LoaderService } from '../../shared/loader.service';
import { CartService } from '../cart/shared/cart.service';
import { Cart } from '../cart/interfaces/cart';
import { SubSink } from 'subsink';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.page.component.html',
  styleUrls: ['./checkout.page.component.scss'],
})
export class CheckoutPageComponent implements OnInit {
  carts: Cart[];
  paymentMethod: string;

  private subs = new SubSink();

  constructor(
    private loader: LoaderService,
    private cart: CartService,
    private router: Router) { }

  ngOnInit() {
    this.loader.show('Calculating Cart...');
    this.subs.sink = this.cart.cart().subscribe(
      cart => {
        this.carts = cart;
        this.loader.dismiss();
      }
    );
  }

  payment(method) {
    this.paymentMethod = method;
  }

  cartTotal(): string {
    return this.calculateCartAmount().toFixed(2);
  }

  total(): string {
    let cartTotal = this.calculateCartAmount();
    cartTotal += this.paymentMethod === 'creditCart' || this.paymentMethod === 'momo' ? 10 : 0;
    return cartTotal.toFixed(2);
  }

  pay() {
    if (this.paymentMethod) {
      this.router.navigate(['/tabs', 'checkout', this.paymentMethod]);
    }
  }

  private calculateCartAmount(): number {
    return this.carts.reduce((sum, cart) => sum + (cart.cartable.price * cart.quantity), 0);
  }
}
