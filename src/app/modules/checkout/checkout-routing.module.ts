import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CheckoutPageComponent } from './checkout.page.component';
import { CreditCardPage } from './pages/credit-card/credit-card.page';
import { MomoPage } from './pages/momo/momo.page';
import { CollectionPage } from './pages/collection/collection.page';

const routes: Routes = [
  {
    path: '',
    component: CheckoutPageComponent,
  },
  {
    path: 'creditCard',
    component: CreditCardPage
  },
  {
    path: 'momo',
    component: MomoPage
  },
  {
    path: 'collection',
    component: CollectionPage
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CheckoutRoutingModule { }
