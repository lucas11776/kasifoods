import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SubSink } from 'subsink';

import { User } from '../authentication/interfaces/authentication';
import { LoaderService } from '../../shared/loader.service';
import { AuthenticationService } from '../authentication/shared/authentication.service';
import { TokenService } from './../authentication/shared/token.service';
import { ToastService } from './../../shared/toast.service';

@Component({
  selector: 'app-profile',
  templateUrl: 'profile.page.html',
  styleUrls: ['profile.page.scss']
})
export class ProfilePage implements OnInit, OnDestroy {
  user: User;

  private subs = new SubSink();

  constructor(
    private loader: LoaderService,
    private auth: AuthenticationService,
    private toast: ToastService,
    private token: TokenService,
    private router: Router) {}

  ngOnInit(): void {
    this.loader.show('Getting Profile...');
    this.subs.sink = this.auth.user().subscribe(
      user => {
        this.user = user;
        this.loader.dismiss();
      },
      error => {
        console.log('Profile error: ', error);
        this.loader.dismiss();
      }
    );
  }

  logout() {
    this.loader.show('Signing Out...');
    this.subs.sink = this.auth.logout().subscribe(
      response => {
        this.token.delete();
        this.toast.show(response.message);
        this.loader.dismiss();
        this.router.navigate(['/authentication']);
      },
      error => {
        console.log('Profile Logout Error: ', error);
        this.loader.dismiss();
      }
    );
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
