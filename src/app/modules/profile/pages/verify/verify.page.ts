/* eslint-disable @typescript-eslint/naming-convention */
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SubSink } from 'subsink';

import { LoaderService } from './../../../../shared/loader.service';
import { AuthenticationService } from '../../../authentication/shared/authentication.service';
import { ToastService } from './../../../../shared/toast.service';

@Component({
  templateUrl: './verify.page.html',
  styleUrls: ['./verify.page.css']
})
export class VerifyPage implements OnInit, OnDestroy {
  form: FormGroup;

  private subs = new SubSink();

  constructor(
    private formBuilder: FormBuilder,
    private loader: LoaderService,
    private authentication: AuthenticationService,
    private toast: ToastService,
    private router: Router) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      id_number: [null, [Validators.required, Validators.minLength(13), Validators.maxLength(13)]]
    });
  }

  verify(): void {
    this.loader.show('Verifying ID number...');
    this.subs.sink = this.authentication.verifyAccount(this.form.value).subscribe(
      response => {
        this.loader.dismiss();
        this.toast.show(response.message);
        this.router.navigate(['/tabs', 'profile']);
      },
      error => {
        console.log(error);
        this.loader.dismiss();
        this.toast.show(error.error.message);
      }
    );
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
