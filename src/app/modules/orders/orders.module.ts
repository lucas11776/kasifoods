import { IonicModule } from '@ionic/angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrdersRoutingModule } from './orders-routing.module';
import { OrdersPageComponent } from './orders.page.component';
import { OrderPage } from './pages/order/order.page';


@NgModule({
  declarations: [
    OrdersPageComponent,
    OrderPage,
  ],
  imports: [
    IonicModule,
    CommonModule,
    OrdersRoutingModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class OrdersModule { }
