/* eslint-disable @typescript-eslint/naming-convention */

import { Food } from '../../foods/interfaces/food';
import { Transaction } from './../../checkout/interfaces/payments';
import { Pagination } from '../../../interfaces/pagination';

export interface Item {
	id: number;
	order_id: number;
	orderable_id: number;
	orderable_type: string;
	status: string;
	quantity: number;
	price: number;
	created_at: string;
	updated_at: string;
	orderable: Food;
}

export interface Order {
	id: number;
	user_id: number;
	transaction_id: number;
	status: string;
	quantity: number;
	amount: number;
	created_at: string;
	updated_at: string;
	transaction: Transaction;
	items?: Item[];
}

export interface Orders extends Pagination {
	data: Order[];
}
