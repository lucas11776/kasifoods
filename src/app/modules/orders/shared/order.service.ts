import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

import { Order, Orders } from '../interfaces/order';
import { API } from '../../../../environments/environment';
import { HttpService } from '../../../shared/http.service';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http: HttpService) { }

  orders(page: number = 1): Observable<Orders> {
    return this.http.get<Orders>(`${API}/orders?page=${page}`);
  }

  order(order: number): Observable<Order> {
    return this.http.get<Order>(`${API}/orders/${order}`);
  }
}
