import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { OrdersPageComponent } from './orders.page.component';
import { OrderPage } from './pages/order/order.page';

const routes: Routes = [
  {
    path: '',
    component: OrdersPageComponent
  },
  {
    path: ':id',
    component: OrderPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrdersRoutingModule { }
