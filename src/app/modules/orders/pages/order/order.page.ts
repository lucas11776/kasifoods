import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SubSink } from 'subsink';

import { LoaderService } from '../../../../shared/loader.service';
import { Order } from '../../interfaces/order';
import { OrderService } from './../../shared/order.service';

@Component({
  templateUrl: './order.page.html',
  styleUrls: ['./order.page.css']
})
export class OrderPage implements OnInit {
  order: Order;

  private subs = new SubSink();

  constructor(
    private loader: LoaderService,
    private route: ActivatedRoute,
    private orderServ: OrderService) { }

  ngOnInit(): void {
    this.loader.show('Getting Order...');
    this.subs.sink = this.route.params.subscribe(params => {
      this.subs.sink = this.orderServ.order(params.id).subscribe(
        order => {
          this.order = order;
          this.loader.dismiss();
        },
        error => {
          console.log('Single Order Error', error);
          this.loader.dismiss();
        }
      );
    });
  }

}
