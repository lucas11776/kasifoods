import { Component, OnDestroy, OnInit } from '@angular/core';
import { SubSink } from 'subsink';

import { Orders } from './interfaces/order';
import { LoaderService } from '../../shared/loader.service';
import { OrderService } from './shared/order.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.page.component.html',
  styleUrls: ['./orders.page.component.scss'],
})
export class OrdersPageComponent implements OnInit, OnDestroy {
  orders: Orders;

  private subs = new SubSink();

  constructor(
    private loader: LoaderService,
    private order: OrderService) { }

    ngOnInit(): void {
      this.loader.show('Getting Orders...');
      this.subs.sink = this.order.orders().subscribe(
        orders => {
          this.orders = orders;
          this.loader.dismiss();
        },
        error => {
          this.loader.dismiss();
          console.log('Orders', error);
        }
      );
    }

    refreshOrders(event): void {
      this.subs.sink = this.order.orders().subscribe(
        orders => {
          this.orders = orders;
          event.target.complete();
        },
        error => {
          console.log('Orders', error);
          event.target.complete();
        }
      );
    }

    ngOnDestroy(): void {
      this.subs.unsubscribe();
    }
}
