
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, Router } from '@angular/router';

import { WelcomeService } from './../shared/welcome.service';

@Injectable({
  providedIn: 'root'
})
export class LoadedGuard implements CanActivate {
  constructor(
    private welcome: WelcomeService,
    private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    return this.welcome.loaded().then(loaded => {
      if (loaded) {
        this.router.navigate(['authentication']);
      }
      return !loaded;
    });
  }
}
