import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { WelcomeService } from './shared/welcome.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.page.component.html',
  styleUrls: ['./welcome.page.component.scss'],
})
export class WelcomePageComponent {
  constructor(
    private welcome: WelcomeService,
    private router: Router) { }

  async authentication(): Promise<any> {
    await this.welcome.mark();
    this.router.navigate(['authentication']);
  }
}
