import { Injectable } from '@angular/core';

import { StorageService } from '../../../shared/storage.service';

@Injectable({
  providedIn: 'root'
})
export class WelcomeService {
  constructor(private storage: StorageService) { }

  async loaded(): Promise<boolean> {
    return await this.storage.get('app_first_load').then(loaded => loaded ? loaded : false);
  }

  async mark(): Promise<any> {
    return await this.storage.set('app_first_load', true);
  }
}
