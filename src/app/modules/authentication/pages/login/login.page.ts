import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { RxwebValidators } from '@rxweb/reactive-form-validators';
import { SubSink } from 'subsink';

import { LoaderService } from '../../../../shared/loader.service';
import { AuthenticationService } from '../../shared/authentication.service';
import { TokenService } from '../../shared/token.service';

@Component({
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css']
})
export class LoginPage implements OnInit, OnDestroy {
  form: FormGroup;
  private subs = new SubSink();

  constructor(
    private formBuilder: FormBuilder,
    private loader: LoaderService,
    private authentication: AuthenticationService,
    private token: TokenService,
    private router: Router) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      email: [null, [RxwebValidators.required()]],
      password: [null, [RxwebValidators.required()]]
    });
  }

  login(): void {
    this.loader.show('Logging In...');
    this.subs.sink = this.authentication.login(this.form.value).subscribe(
      token => {
        this.token.save(token);
        this.loader.dismiss();
        this.router.navigate(['tabs']);
      },
      error => {
        console.warn(error);
        this.loader.dismiss();
      }
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}
