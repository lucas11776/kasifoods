/* eslint-disable @typescript-eslint/naming-convention */
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { RxwebValidators } from '@rxweb/reactive-form-validators';
import { SubSink } from 'subsink';

import { LoaderService } from '../../../../shared/loader.service';
import { AuthenticationService } from '../../shared/authentication.service';
import { TokenService } from '../../shared/token.service';

@Component({
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.css']
})
export class RegisterPage implements OnInit, OnDestroy {
  form: FormGroup;
  private subs = new SubSink();

  constructor(
    private formBuilder: FormBuilder,
    private loader: LoaderService,
    private authentication: AuthenticationService,
    private token: TokenService,
    private router: Router) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      name: [null, [RxwebValidators.required()]],
      email: [null, [RxwebValidators.required()]],
      password: [null, [RxwebValidators.required()]],
      password_confirmation: [null, []]
    });
  }

  register(): void {
    this.loader.show('Creating Account...');
    this.subs.sink = this.authentication.register(this.form.value).subscribe(
      token => {
        this.token.save(token);
        this.loader.dismiss();
        this.router.navigate(['tabs']);
      },
      error => {
        console.warn(error);
        this.loader.dismiss();
      }
    );
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
