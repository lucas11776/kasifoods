import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';

import { ExploreContainerComponentModule } from '../../explore-container/explore-container.module';
import { AuthenticationRoutingModule } from './authentication-routing.module';

import { AuthenticationPage } from './authentication.page';
import { LoginPage } from './pages/login/login.page';
import { RegisterPage } from './pages/register/register.page';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RxReactiveFormsModule,
    ExploreContainerComponentModule,
    AuthenticationRoutingModule
  ],
  declarations: [
    AuthenticationPage,
    LoginPage,
    RegisterPage
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AuthenticationModule {}
