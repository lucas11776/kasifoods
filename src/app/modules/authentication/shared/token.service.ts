import { Injectable } from '@angular/core';

import { Token } from '../interfaces/authentication';
import { StorageService } from '../../../shared/storage.service';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor(private storage: StorageService) { }

  async save(token: Token): Promise<any> {
    return await this.storage.set('token', token);
  }

  async delete(): Promise<any> {
    return await this.storage.remove('token');
  }

  async token(): Promise<string> {
    return await this.storage.get('token').then((token: Token) => token ? token.access_token : '');
  }
}
