import { TokenService } from './token.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';

import { HttpService } from '../../../shared/http.service';
import { API } from '../../../../environments/environment';
import { Token, User, VerifyAccount } from '../interfaces/authentication';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(
    private http: HttpService,
    private token: TokenService,
    private jwt: JwtHelperService) { }

  register(form): Observable<Token> {
    return this.http.post(`${API}/authentication/register`, form);
  }

  login(credentials): Observable<Token> {
    return this.http.post(`${API}/authentication/login`, credentials);
  }

  logout(): Observable<any> {
    return this.http.post(`${API}/authentication/logout`, {});
  }

  verifyAccount(form: VerifyAccount): Observable<any> {
    return this.http.post(`${API}/user/verify/id`, form);
  }

  user(): Observable<User> {
    return this.http.get<User>(`${API}/user`);
  }

  async auth(): Promise<boolean> {
    return this.token.token().then(token => !this.jwt.isTokenExpired(token));;
  }

  async guest(): Promise<boolean> {
    return this.token.token().then(token => this.jwt.isTokenExpired(token));
  }
}
