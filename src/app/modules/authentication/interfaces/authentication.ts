/* eslint-disable @typescript-eslint/naming-convention */
import { Image } from './../../../interfaces/image';

export interface Token {
	access_token: string;
	access_type: string;
	expires: number;
}

export interface IdNumber {
	name: string;
	surname: string;
}

export interface User {
	id: number;
	name: string;
	email: string;
	email_verified_at: string;
	phone_number: string;
	phone_number_verified_at: string;
	latitude: number;
	longitude: number;
	create_at: string;
	updated_at: string;
	avatar: Image;
	id_number?: IdNumber;
}

export interface VerifyAccount {
	id_number: string;
}
