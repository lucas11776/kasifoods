import { Component, OnDestroy, OnInit } from '@angular/core';
import { SubSink } from 'subsink';

import { Cart } from './interfaces/cart';
import { LoaderService } from '../../shared/loader.service';
import { CartService } from './shared/cart.service';

@Component({
  selector: 'app-cart-page',
  templateUrl: 'cart.page.html',
  styleUrls: ['cart.page.scss']
})
export class CartPage implements OnInit, OnDestroy {
  carts: Cart[];

  private subs = new SubSink();

  constructor(
    private loader: LoaderService,
    private cart: CartService) { }

  ngOnInit(): void {
    this.loader.show('Getting Cart....');
    // Get Cart
    this.subs.sink = this.cart.cart().subscribe(
      cart => {
        this.carts = cart;
        this.loader.dismiss();
      },
      error => this.loader.dismiss()
    );
    // Cart Added Event
    this.subs.sink = this.cart.cartAdded.subscribe(
      cart => this.cartAdded(cart)
    );
    // Cart Updated Event
    this.subs.sink = this.cart.cartUpdated.subscribe(
      cart => this.cartUpdated(cart)
    );
    // Cart Deleted Event
    this.subs.sink = this.cart.cartDeleted.subscribe(
      cart => this.cartRemoved(cart)
    );
  }

  total(): string {
    return this.carts?.reduce((sum, cart) => sum + (cart.cartable.price * cart.quantity), 0).toFixed(2);
  }

  cartAdded(cart: Cart): void {
    this.carts.push(cart);
  }

  cartUpdated(cart: Cart): void {
    this.carts = this.carts.map(c => c.id !== cart.id ? c : cart);
  }

  cartRemoved(cart: Cart): void {
    this.carts = this.carts.filter(c => c.id !== cart.id);
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
