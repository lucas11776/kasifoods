import { Observable, Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';

import { API } from '../../../../environments/environment';
import { Cart } from '../interfaces/cart';
import { HttpService } from '../../../shared/http.service';
import { ToastService } from '../../../shared/toast.service';

export interface Item {
  food?: number;
}

@Injectable({
  providedIn: 'root'
})
export class CartService {
  cartAdded = new Subject<Cart>();
  cartDeleted = new Subject<Cart>();
  cartUpdated = new Subject<Cart>();

  constructor(
    private http: HttpService,
    private toast: ToastService) { }

  cart(): Observable<Cart[]> {
    return this.http.get(`${API}/cart`);
  }

  add(item: Item): Observable<Cart> {
    return this.http.post<Cart>(`${API}/cart`, item).pipe(
      tap(cart => {
        this.cartAdded.next(cart);
        this.toast.show('Item has been added to cart.');
      })
    );
  }

  incrementQuantity(cart: number): Observable<Cart> {
    return this.http.post<Cart>(`${API}/cart/${cart}/add`, {}).pipe(
      tap(c => {
        this.cartUpdated.next(c);
        this.toast.show('Item quantity has been increased in cart.');
      })
    );
  }

  decrementQuantity(cart: number): Observable<Cart> {
    return this.http.post<Cart>(`${API}/cart/${cart}/sub`, {}).pipe(
      tap(c => {
        this.cartUpdated.next(c);
        this.toast.show('Item quantity has been decreased in cart.');
      })
    );
  }

  remove(cart: Cart): Observable<any> {
    return this.http.delete(`${API}/cart/${cart.id}`, {}).pipe(
      tap(response => {
        this.cartDeleted.next(cart);
        this.toast.show(response.message);
      })
    );
  }
}
