/* eslint-disable @angular-eslint/no-output-rename */
/* eslint-disable @angular-eslint/no-input-rename */
import { Component, EventEmitter, Input, OnDestroy, Output } from '@angular/core';
import { SubSink } from 'subsink';

import { Cart } from '../../interfaces/cart';
import { CartService } from '../../shared/cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnDestroy {
  @Input('cart') cart: Cart;
  @Output('deleted') deleted = new EventEmitter<Cart>();
  @Output('updated') updated = new EventEmitter<Cart>();

  private subs = new SubSink();

  constructor(private cartService: CartService) { }

  incrementCart(): void {
    this.subs.sink = this.cartService.incrementQuantity(this.cart.id).subscribe(
      //cart => this.cartService.updated(cart) //this.updated.emit(cart)
    );
  }

  decrementCart(): void {
    this.subs.sink = this.cartService.decrementQuantity(this.cart.id).subscribe(
      //cart => this.cartService.deleted(cart) //this.updated.emit(cart)
    );
  }

  removeCart(): void {
    this.subs.sink = this.cartService.remove(this.cart).subscribe(
      response => console.log(response)
    );
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
