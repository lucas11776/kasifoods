/* eslint-disable @typescript-eslint/naming-convention */
import { Food } from '../../foods/interfaces/food';

export interface Item {
	food?: number;
}

export interface Cart {
	id: number;
	user_id: number;
	cartable_id: string;
	quantity: number;
	created_at: string;
	update_at: string;
	cartable: Food;
}
