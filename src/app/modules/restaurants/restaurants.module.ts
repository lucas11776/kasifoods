import { IonicModule } from '@ionic/angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { RestaurantsRoutingModule } from './restaurants-routing.module';
import { FoodsModule } from '../foods/foods.module';

import { RestaurantsPage } from './restaurants.page';
import { ExploreContainerComponentModule } from '../../explore-container/explore-container.module';
import { StorePage } from './pages/store/store.page';
import { LocationPage } from './pages/location/location.page';
import { OrderPage } from './pages/order/order.page';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ExploreContainerComponentModule,
    RestaurantsRoutingModule,
    FoodsModule
  ],
  declarations: [RestaurantsPage, StorePage, LocationPage, OrderPage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RestaurantsPageModule {}
