
/* eslint-disable @typescript-eslint/naming-convention */
import { Pagination } from 'src/app/interfaces/pagination';
import { Image } from '../../../interfaces/image';
import { Address } from '../../../interfaces/address';
import { Food } from '../../foods/interfaces/food';
import { User } from '../../authentication/interfaces/authentication';

export interface Store {
	id: number;
	latitude: number;
	longitude: number;
	name: string;
	slug: string;
	description: string;
	created_at: string;
	updated_at: string;
	image: Image;
	price: string;
	phone_number: string;
	address: Address;
	user?: User;
	foods?: Food[];
};

export interface Stores extends Pagination {
	data: Store[];
};
