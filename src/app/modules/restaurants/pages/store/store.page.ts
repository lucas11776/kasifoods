import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SubSink } from 'subsink';

import { Store } from '../../interfaces/store';
import { LoaderService } from '../../../../shared/loader.service';
import { StoreService } from '../../shared/store.service';

@Component({
  templateUrl: './store.page.html',
  styleUrls: ['./store.page.css']
})
export class StorePage implements OnInit, OnDestroy {
  store: Store;

  private subs = new SubSink();

  constructor(
    private loader: LoaderService,
    private route: ActivatedRoute,
    private storeServ: StoreService) { }

  ngOnInit(): void {
    this.loader.show('Getting Store...');
    this.subs.sink = this.route.params.subscribe(
      params => {
        this.subs.sink = this.storeServ.store(params.id).subscribe(
          store => {
            this.store = store;
            this.loader.dismiss();
          },
          error => {
            this.loader.dismiss();
          }
        );
      }
    );
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
