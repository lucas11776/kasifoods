import { Store } from './../../interfaces/store';
import { OnDestroy } from '@angular/core';
/* eslint-disable @typescript-eslint/type-annotation-spacing */
import { Component, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SubSink } from 'subsink';

import { LoaderService } from '../../../../shared/loader.service';
import { StoreService } from '../../shared/store.service';

declare let H: any;

export interface Coordinates {
  latitude: number;
  longitude: number;
}

@Component({
  templateUrl: './location.page.html',
  styleUrls: ['./location.page.css']
})
export class LocationPage implements AfterViewInit, OnDestroy {
  @ViewChild('map') map: ElementRef;

  private subs = new SubSink();

  constructor(
    private route: ActivatedRoute,
    private loader: LoaderService,
    private store: StoreService) { }

  ngAfterViewInit() {
    // Request geolocation Permissions
    if (navigator.geolocation) {
      this.loader.show('Getting Store Location...');
      this.route.params.subscribe(
        params => {
          this.subs.sink = this.store.store(params.id).subscribe(
            store => {
              this.showMap(store);
              this.loader.dismiss();
            },
            error => {
              this.loader.dismiss();
            }
          );
        }
      );
      // navigator.geolocation.getCurrentPosition(this.showMap);
    }
  }

  showMap(store: Store): void {
    const platform = new H.service.Platform({
      apikey: '2LOvRfsHVASDpWpmE1g6vn6xlI8VrUYyGej_v659cNo'
    });

    const defaultLayers = platform.createDefaultLayers();

    const map = new H.Map(document.getElementById('map-location-store'), defaultLayers.vector.normal.map, {
      center: { lat: store.latitude, lng: store.longitude },
      pixelRatio: window.devicePixelRatio || 1,
      zoom: 17,
    });

    window.addEventListener('resize', () => map.getViewPort().resize());

    const behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));
    const ui = H.ui.UI.createDefault(map, defaultLayers);
    const parisMarker = new H.map.Marker({lat: store.latitude, lng: store.longitude});

    map.addObject(parisMarker);

    setTimeout(() => window.dispatchEvent(new Event('resize')), 2500);
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
