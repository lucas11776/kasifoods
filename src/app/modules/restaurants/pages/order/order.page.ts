import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SubSink } from 'subsink';

import { LoaderService } from '../../../../shared/loader.service';
import { StoreService } from '../../shared/store.service';
import { ToastService } from '../../../../shared/toast.service';

@Component({
  templateUrl: './order.page.html',
  styleUrls: ['./order.page.css']
})
export class OrderPage implements OnInit, OnDestroy {
  form: FormGroup;
  params: any;

  private subs = new SubSink();

  constructor(
    private formBuilder: FormBuilder,
    private loader: LoaderService,
    private store: StoreService,
    private toast: ToastService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      message: [null, [Validators.required, Validators.minLength(30), Validators.maxLength(255)]]
    });
    this.subs.sink = this.route.params.subscribe(
      params => this.params = params
    );
  }

  send(): void {
    this.loader.show('Sending Order...');
    this.subs.sink = this.store.order(this.params.id, this.form.value).subscribe(
      response => {
        this.form.reset();
        this.loader.dismiss();
        this.toast.show(response.message);
        this.router.navigate(['/tabs', 'restaurants', this.params.id]);
      },
      error => {
        this.loader.dismiss();
        this.toast.show(error.error.message);
      }
    );
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
