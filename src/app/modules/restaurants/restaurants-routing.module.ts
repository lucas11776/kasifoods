import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RestaurantsPage } from './restaurants.page';
import { StorePage } from './pages/store/store.page';
import { LocationPage } from './pages/location/location.page';
import { OrderPage } from './pages/order/order.page';

const routes: Routes = [
  {
    path: '',
    component: RestaurantsPage,
  },
  {
    path: ':id',
    component: StorePage
  },
  {
    path: ':id/location',
    component: LocationPage,
  },
  {
    path: ':id/special-order',
    component: OrderPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RestaurantsRoutingModule {}
