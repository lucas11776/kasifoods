import { Injectable } from '@angular/core';
import {  } from '@ionic-native/http';
import { Observable } from 'rxjs';

import { API } from '../../../../environments/environment';
import { Stores, Store } from '../interfaces/store';
import { HttpService } from '../../../shared/http.service';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  constructor(private http: HttpService) { }

  stores(page: number = 1): Observable<Stores> {
    return this.http.get(`${API}/stores?page=${page} `);
  }

  store(store: number): Observable<Store> {
    return this.http.get(`${API}/stores/${store}`);
  }

  order(store: number , message: {message: string}): Observable<any> {
    return this.http.post(`${API}/stores/${store}/special-order`, message);
  }
}
