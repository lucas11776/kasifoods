import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { SubSink } from 'subsink';

import { LoaderService } from '../../shared/loader.service';
import { Stores } from './interfaces/store';
import { StoreService } from './shared/store.service';

@Component({
  selector: 'app-restaurants',
  templateUrl: 'restaurants.page.html',
  styleUrls: ['restaurants.page.scss']
})
export class RestaurantsPage implements OnInit, OnDestroy {
  @ViewChild('rating') rating: any;

  stores: Stores;

  private subs = new SubSink();

  constructor(
    private loader: LoaderService,
    private store: StoreService) {}

  ngOnInit(): void {
    this.loader.show('Getting Restaurants...');
    this.subs.sink = this.store.stores().subscribe(
      stores => {
        this.stores = stores;
        this.loader.dismiss();
      },
      error => {
        console.warn('Restaurant error: ', error);
        this.loader.dismiss();
      }
    );
  }

  getRestaurants(events): void {
    this.subs.sink = this.store.stores(this.stores.current_page + 1).subscribe(
      stores => {
        stores.data = [...this.stores.data, ...stores.data];
        this.stores = stores;
        events.target.complete();
      }
    );
  }

  ngOnDestroy(): void {

  }
}
