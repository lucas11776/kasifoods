/* eslint-disable @angular-eslint/no-input-rename */
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Food } from '../../interfaces/food';
import { SubSink } from 'subsink';

import { CartService } from '../../../cart/shared/cart.service';

@Component({
  selector: 'app-food',
  templateUrl: './food.component.html',
  styleUrls: ['./food.component.css']
})
export class FoodComponent implements OnInit, OnDestroy {
  @Input('food') food: Food;

  private subs = new SubSink();

  constructor(private cart: CartService) { }

  ngOnInit(): void {
  }

  addCart(): void {
    this.subs.sink = this.cart.add({food: this.food.id}).subscribe(
      cart => this.food.cart = cart
    );
  }

  removeCart(): void {
    this.subs.sink = this.cart.remove(this.food.cart).subscribe(
      response => this.food.cart = null
    );
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
