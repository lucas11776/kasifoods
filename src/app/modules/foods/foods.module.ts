import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicRatingComponentModule } from 'ionic-rating-component';

import { ExploreContainerComponentModule } from '../../explore-container/explore-container.module';

import { Tab3PageRoutingModule } from './foods-routing.module';
import { FoodsPage } from './foods.page';
import { FoodComponent } from './components/food/food.component';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    RouterModule.forChild([{ path: '', component: FoodsPage }]),
    Tab3PageRoutingModule,
    IonicRatingComponentModule
  ],
  declarations: [FoodsPage, FoodComponent],
  exports: [FoodComponent]
})
export class FoodsModule {}
