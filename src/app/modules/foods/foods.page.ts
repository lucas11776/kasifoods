import { Component, OnDestroy, OnInit } from '@angular/core';
import { SubSink } from 'subsink';

import { LoaderService } from '../../shared/loader.service';
import { Foods } from './interfaces/food';
import { FoodService } from './shared/food.service';
import { CartService } from '../cart/shared/cart.service';
import { Cart } from '../cart/interfaces/cart';

@Component({
  selector: 'app-foods',
  templateUrl: 'foods.page.html',
  styleUrls: ['foods.page.scss']
})
export class FoodsPage implements OnInit, OnDestroy {
  foods: Foods;

  private subs = new SubSink();

  constructor(
    private loader: LoaderService,
    private food: FoodService,
    private cart: CartService) {}

  ngOnInit(): void {
    this.loader.show('Getting Foods...');
    // Get Food From Database
    this.subs.sink = this.food.foods().subscribe(
      foods => {
        this.foods = foods;
        this.loader.dismiss();
      },
      error => {
        console.log('Foods error: ', error);
        this.loader.dismiss();
      }
    );
    // Cart Deleted Event
    this.subs.sink = this.cart.cartDeleted.subscribe(
      cart => this.cartDeleted(cart)
    );
  }

  getFoods(event): void {
    this.subs.sink = this.food.foods(this.foods.current_page+1).subscribe(
      foods => {
        foods.data = [...this.foods.data, ...foods.data];
        this.foods = foods;
        event.target.complete();
      },
      error => console.log('Get Foods Error: ', error)
    );
  }

  cartDeleted(cart: Cart): void {
    const foods = this.foods.data.map(food => {
      if (food.cart && food.cart.id === cart.id) {
        food.cart = null;
        return food;
      }
      return food;
    });
    this.foods.data = foods;
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
