import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { API } from '../../../../environments/environment';
import { HttpService } from '../../../shared/http.service';

import { Foods } from '../interfaces/food';

@Injectable({
  providedIn: 'root'
})
export class FoodService {

  constructor(private http: HttpService) { }

  foods(page: number = 1): Observable<Foods> {
    return this.http.get(`${API}/foods?page=${page}`);
  }
}
