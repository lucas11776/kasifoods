/* eslint-disable @typescript-eslint/naming-convention */
import { Pagination } from 'src/app/interfaces/pagination';
import { Image } from './../../../interfaces/image';
import { Cart } from '../../cart/interfaces/cart';

export interface Food {
	id: number;
	name: string;
	slug: string;
	price: number;
	description: string;
	created_at: string;
	updated_at: string;
	image: Image;
	cart?: Cart;
}

export interface Foods extends Pagination {
	data: Food[];
}
