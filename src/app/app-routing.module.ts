import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

import { WelcomeGuard } from './modules/welcome/guards/welcome.guard';
import { LoadedGuard } from './modules/welcome/guards/loaded.guard';
import { GuestGuard } from './modules/authentication/guards/guest.guard';
import { AuthGuard } from './modules/authentication/guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule),
    canActivate: [WelcomeGuard, AuthGuard]
  },
  {
    path: 'welcome',
    loadChildren: () => import('./modules/welcome/welcome.module').then(m => m.WelcomeModule),
    canActivate: [GuestGuard, LoadedGuard]
  },
  {
    path: 'authentication',
    loadChildren: () => import('./modules/authentication/authentication.module').then(m => m.AuthenticationModule),
    canActivate: [WelcomeGuard, GuestGuard]
  },
  {
    path: '',
    redirectTo: '/tabs',
    pathMatch: 'full'
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
