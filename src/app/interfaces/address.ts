/* eslint-disable @typescript-eslint/naming-convention */

export interface Address {
	id: number;
	address: string|null;
	city: string|null;
	postal_code: string|null;
	country: string|null;
	created_at: string;
	updated_at: string;
};
