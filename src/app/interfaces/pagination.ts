/* eslint-disable @typescript-eslint/naming-convention */

export interface Pagination {
	current_page: number;
	data: any;
	from: number;
	last_page: number;
	last_page_url: string|null;
	next_page_url: string;
	path: string;
	per_page: number;
	prev_page_url: string|null;
	to: number|null;
	total: number;
};
