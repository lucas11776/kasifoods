/* eslint-disable @typescript-eslint/naming-convention */

export interface Image {
	id: number;
	path: string|null;
	url: string;
	created_at: string;
	updated_at: string;
}
