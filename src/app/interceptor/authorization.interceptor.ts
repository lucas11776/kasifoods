/* eslint-disable @typescript-eslint/naming-convention */

import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { from, Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { TokenService } from '../modules/authentication/shared/token.service';

@Injectable()
export class AuthorizationInterceptor implements HttpInterceptor {

  constructor(private token: TokenService) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return from(this.token.token()).pipe(
      switchMap(token => {
        const requestClone = request.clone({
          setHeaders: { Authorization: `Bearer ${token}` }
        });
        return next.handle(requestClone);
      })
    );
  }

}
