import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(private toast: ToastController) { }

  async show(msg: string, time: number = 3000): Promise<any> {
    const toast = await this.toast.create({
      message: msg,
      duration: time,
      color: 'primary'
    });
    toast.present();
  }
}
