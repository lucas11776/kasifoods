import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage-angular';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  private store: Storage | null = null;

  constructor(private storage: Storage) {
    this.init();
  }

  async init() {
    const storage = await this.storage.create();
    this.store = storage;
    // const l = await this.store.get('app_first_load');
    // console.log('APP STORAGE ', l);
  }

  async set(key: string, value: any): Promise<any> {
    return await this.store?.set(key, value);
  }

  async get(key: string) {
    return await this.store?.get(key);
  }

  async remove(key: string): Promise<any> {
    return this.store.remove(key);
  }
}
