import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HTTP, HTTPResponse } from '@ionic-native/http/ngx';
import { from, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) { }

  get<T>(url: string, data: any = {}): Observable<T>  {
    return this.http.get<T>(url);

  }

  post<T>(url: string, data: any): Observable<T> {
    return this.http.post<T>(url, data);
  }

  delete<T>(url: string, data: any): Observable<T> {
    return this.http.delete<T>(url);
  }

  patch<T>(url: string, data: any): Observable<T> {
    return this.http.patch<T>(url, data);
  }

  protected resolve(response: Promise<HTTPResponse>): Promise<any> {
    return response
      .then(responseSuccess => JSON.parse(responseSuccess.data))
      .catch(responseError => {
        throw JSON.parse(responseError.error);
      });
  }
}
