import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  private loading: HTMLIonLoadingElement;

  constructor(private loader: LoadingController) { }

  async show(msg: string = 'Please Wait...'): Promise<any> {
    this.loading = await this.loader.create({message: msg, spinner: 'bubbles'});
    this.loading.present();
  }

  async dismiss() {
    await this.loading?.dismiss();
  }
}
