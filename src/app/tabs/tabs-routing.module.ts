import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'restaurants',
        loadChildren: () => import('../modules/restaurants/restaurants.module').then(m => m.RestaurantsPageModule)
      },
      {
        path: 'foods',
        loadChildren: () => import('../modules/foods/foods.module').then(m => m.FoodsModule)
      },
      {
        path: 'cart',
        loadChildren: () => import('../modules/cart/cart.module').then(m => m.Tab2PageModule)
      },
      {
        path: 'checkout',
        loadChildren: () => import('../modules/checkout/checkout.module').then(m => m.CheckoutModule)
      },
      {
        path: 'profile',
        loadChildren: () => import('../modules/profile/profile.module').then(m => m.Tab3PageModule)
      },
      {
        path: 'orders',
        loadChildren: () => import('../modules/orders/orders.module').then(m => m.OrdersModule)
      },
      {
        path: '',
        redirectTo: '/tabs/restaurants',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/restaurants',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class TabsPageRoutingModule {}
